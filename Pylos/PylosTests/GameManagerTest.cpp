#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Pylos/GameManager.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Testing
{
	TEST_CLASS(GameManagerTests)
	{
	public:
		TEST_METHOD(checkColorPlayer1)
		{
			GameManager game;
			Assert::IsTrue(game.player1.color == Piece::Color::White);
		}

		TEST_METHOD(switchPlayer)
		{
			GameManager game;
			game.currentPlayer = &game.player1;
			game.switchPlayer();
			if (game.currentPlayer == &game.player1)
				Assert::Fail();
		}

		TEST_METHOD(CheckColorPlayer2)
		{
			GameManager game;
			Assert::IsTrue(game.player2.color == Piece::Color::Black);
		}

		TEST_METHOD(moveTest)
		{
			GameManager gameManager;
			// Piece to be moved
			// Setting pieces available
			gameManager.game.board[0].second.setColor(Piece::Color::White);
			gameManager.game.board[1].second.setColor(Piece::Color::Black);
			gameManager.game.board[2].second.setColor(Piece::Color::Black);
			gameManager.game.board[5].second.setColor(Piece::Color::Black);
			gameManager.game.board[6].second.setColor(Piece::Color::Black);

			// Upper Piece 
			gameManager.game.board[16].first.availability = true;
			gameManager.movePieceFromBoardToUpperLevel(0, 16);
			Assert::IsTrue(gameManager.game.board[0].second.getColor() == Piece::Color::None && gameManager.game.board[16].second.getColor() == Piece::Color::White);
		}

		TEST_METHOD(takeTest)
		{
			GameManager gameManager;
			// Stores the old piece count
			int oldCurrentPlayerPieceCount = gameManager.currentPlayer->countPieces;
			// Created a square
			gameManager.game.board[1].second.setColor(Piece::Color::White);
			gameManager.game.board[2].second.setColor(Piece::Color::White);
			gameManager.game.board[5].second.setColor(Piece::Color::White);
			gameManager.game.board[6].second.setColor(Piece::Color::White);
			// Takes the piece at index 6
			gameManager.takePiecesFromBoard(6);
			Assert::IsTrue(gameManager.game.board[6].second.getColor() == Piece::Color::None && gameManager.currentPlayer->countPieces == oldCurrentPlayerPieceCount + 1);
		}

		TEST_METHOD(placeTest)
		{
			GameManager gameManager;
			int oldCurrentPlayerPieceCount = gameManager.currentPlayer->countPieces;
			// Places a piece at index 0
			gameManager.placePieceFromReserve(0);
			Assert::IsTrue(gameManager.game.board[0].second.getColor() == Piece::Color::White && gameManager.currentPlayer->countPieces == oldCurrentPlayerPieceCount - 1);
		}

		TEST_METHOD(WinBlack)
		{
			GameManager gameManager;
			gameManager.game.board[29].first.availability = true;

			gameManager.game.board[29].second.setColor(Piece::Color::Black);
			if (gameManager.game.board[29].second.getColor() == Piece::Color::White && gameManager.foundWinner == true)
				Assert::Fail();
		}

		TEST_METHOD(WinWhite)
		{
			GameManager gameManager;
			gameManager.game.board[29].first.availability = true;
			gameManager.game.board[29].second.setColor(Piece::Color::White);
			if (gameManager.game.board[29].second.getColor() == Piece::Color::Black && gameManager.foundWinner == true)
				Assert::Fail();
		}

		TEST_METHOD(getIndexFromCoordinates)
		{
			std::string coordinates = "1,1,1";
			GameManager gameManager;
			int index = gameManager.getIndexFromCoordinates(coordinates);
			Assert::AreEqual(0, index);
		} 

		TEST_METHOD(fixSquareBelowTest)
		{
			GameManager gameManager;
			gameManager.game.board[16].first.availability = true;
			gameManager.game.board[16].second.setColor(Piece::Color::Black);
			gameManager.fixSquareBelow(16, false);
			Assert::IsTrue(gameManager.game.board[0].first.availability == 0 && gameManager.game.board[1].first.availability == 0 && gameManager.game.board[4].first.availability == 0 && gameManager.game.board[5].first.availability == 0);
		}
		TEST_METHOD(checkMovablePiece)
		{
			GameManager gameManager;
			gameManager.game.board[0].second.setColor(Piece::Color::White);
			gameManager.game.board[16].first.availability = true;
			Assert::IsTrue(gameManager.checkMoveablePiece(0));
		}

	};
}