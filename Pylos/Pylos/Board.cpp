#include "Board.h"

//Building the empty board
Board::Board()
{
	// Making the pieces Blank and Not Avaiable

	buildAllLayers();
}

Board::~Board()
{
	//empty
}

void Board::buildAllLayers()
{
	Piece emptyPiece(Piece::Color::None);
	int i = 0; // pieceIndex
	int level = 1; // levelIndex
	while (i < 30 && layersCount != 0) // while Build is not finished
	{
		for (unsigned int row = 1; row <= layersCount; ++row)
		{
			PieceInfo.row = row;
			PieceInfo.level = level;
			for (unsigned int column = 1; column <= layersCount; ++column)
			{
				PieceInfo.column = column;
				if (i == 0)
				{
					PieceInfo.availability = 1; // <- making the availability of the first level true
				}
				if (i == 16)
				{
					PieceInfo.availability = 0;
				}
				// For debugging
				//std::cout << "Coordinates: " << row << ' ' << column << ' ' << level << ' ';
				//pieces[i].getStatus() ? (std::cout << "Y" << std::endl) : (std::cout << "X" << std::endl);
				// Adding the empty spaces on the board
				board[i] = std::make_pair(PieceInfo, emptyPiece);
				i++;
			}
		}
		level++; // Going one level up
		layersCount--;
	}
	layersCount = 4; // Resetting layersCount back to 4
}

void Board::outputAllLayers() const
{
	int index = 0;
	while (layersCount != 0) // while Output is not finished
	{
		std::cout << "     ";
		for (int j = 1; j <= layersCount; ++j)
			std::cout << j << "     ";
		std::cout << std::endl << "  " << "------------------------" << std::endl;

		for (int k = 0; k < layersCount; ++k)
		{
			std::cout << k + 1 << '|' << ' ';
			// printing every piece per row
			for (int piecesPerRow = 0; piecesPerRow < layersCount; piecesPerRow++)
			{
				//Debugging
				//std::cout << "  " << index << "  ";
				//std::cout << "S=" << board[index].first.availability << " ";
				if (board[index].first.availability == false && board[index].second.getColor() == Piece::Color::None)
				{
					std::cout << "( X ) ";
					index++;
				}
				else
					std::cout << board[index++].second << ' ';
			}
			std::cout << std::endl;
		}
		layersCount--;
		std::cout << std::endl;
	}
	layersCount = 4;
}

void Board::checkSquare()
{
	bool A = false;
	bool B = false;

	//Down Row
	int nextRowIndex = 4;
	// secondLevel starts at 16
	int secondLevelIndex = 16;
	// thirdLevel starts at 25
	int thirdLevelIndex = 25;
	// Number of pieces for every level
	int pieceCount[4] = { 16, 9, 4, 1 };
	// Current level
	int level = 1;

	//checking for squares
	for (int i = 0; i <= thirdLevelIndex; ++i)
	{
		if (i == 11 || i == 21) // We skip the last column for every level
		{
			i += nextRowIndex;
			i++;
			nextRowIndex--;
			level++;
		}

		if (i != 3 && i != 7 && i != 18 && i != 26)
		{
			if (board[i].second.getColor() != Piece::Color::None  && board[i + 1].second.getColor() != Piece::Color::None)
				A = true;
			if (board[i + nextRowIndex].second.getColor() != Piece::Color::None && board[i + 1 + nextRowIndex].second.getColor() != Piece::Color::None)
				B = true;


			//Index of the piece above the square
			int squareUpIndex = i + 1 + pieceCount[level - 1] - board[i].first.row;
			//check for squares
			if (A == true && B == true)
			{
				//std::cout << "Piece before: " <<  board[squareUpIndex].second << ' ' << std::endl;
				//std::cout << "We have a square at index: " << squareUpIndex << std::endl;
				//Debugging
				//std::cout << "i = " << i << " level = " << level << " pieceCount[level] = " << pieceCount[level] << " Column = " << board[i].first.column << std::endl;

				if (board[squareUpIndex].first.availability == false && board[squareUpIndex].second.getColor() == Piece::Color::None)
					board[squareUpIndex].first.availability = true;

				if (board[squareUpIndex].second.getColor() != Piece::Color::None)
				{
					board[i].first.availability = false;
					board[i + 1].first.availability = false;
					board[i + nextRowIndex].first.availability = false;
					board[i + 1 + nextRowIndex].first.availability = false;
				}

				//std::cout << "Piece after: " << board[squareUpIndex].second << std::endl;
			}

			else
			{
				if (board[squareUpIndex].first.availability == true && board[squareUpIndex].second.getColor() == Piece::Color::None)
					board[squareUpIndex].first.availability = false;
				//std::cout << "No square" << std::endl;
			}
		}
		A = B = false;
	}
}

bool Board::checkForTakeablePieces(const Piece::Color & color) const
{
	for (int i = 0; i < board.size(); ++i)
	{
		if (board[i].second.getColor() == color && board[i].first.availability == true)
			return true;
	}
	return false;
}



