#pragma once

#include <iostream>
#include <array>
#include "Piece.h"

class Board
{
public:
	//Every Piece on the Board will have a coordonate (Row,Column,Level)
	struct Info
	{
		unsigned int column;
		unsigned int row;
		unsigned int level;
		bool availability = 0;
	}PieceInfo;

	//The Board
	std::array<std::pair<Info, Piece>, 30> board;

	// How Many layers a board has
	mutable uint8_t layersCount = 4;

public:
	Board(); //default constructor
	~Board(); //destructor

	//Builds the Layers
	void buildAllLayers();

	//Outputs the Layers
	void outputAllLayers() const;

	//Method to check if squares exists
	void checkSquare();

	//Check movable pieces
	bool checkForTakeablePieces(const Piece::Color& color) const;
};
