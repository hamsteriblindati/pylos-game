#pragma once
#include <iostream>

class Piece
{
public: 
	//enum for Color types
	enum class Color : uint8_t
	{
		None,
		Black,
		White
	};

public:
	Piece(); //default constructor
	Piece(const Color & color); //second constructor
	Piece(const Piece & other); // copy constructor
	Piece(Piece && other); // move constructor
	~Piece(); //destructor

	//getters
	Color getColor() const;
	//setters
	void setColor(const Color& color);
	//operator overloading
	Piece & operator = (const Piece & other);
	Piece & operator = (Piece && other); // <-- see move semantics
	friend std::ostream& operator << (std::ostream& os, const Piece& piece);


private:
	 Color m_color;
};


