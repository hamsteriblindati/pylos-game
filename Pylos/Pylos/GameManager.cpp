#include "GameManager.h"

GameManager::GameManager()
{
	// Player2 gets number 2
	player2.number = 2;

	// Player2 gets Black Pieces
	player2.color = Piece::Color::Black;
}

GameManager::~GameManager()
{
}

void GameManager::playGame()
{
	std::ofstream of("Pylos.log", std::ios::app);
	Logger logger(of, Logger::Level::Info);
	logger.log("Started Application...", Logger::Level::Info);

	while (game.board[29].second.getColor() == Piece::Color::None) // While there is no winner
	{
		//std::cin.get();
		system("cls");
		game.checkSquare();
		game.outputAllLayers();
		std::cout << "Correct commands : place | take | move | help | exit" << std::endl;
		std::cout << "It's Player " << currentPlayer->number << "'s turn." << std::endl;
		std::cout << "Current player pieces = " << currentPlayer->countPieces << " " << std::endl;
		std::cout << "Enter a command: ";
		playerLastCommand = getPlayerInput();
		// After input do something based on input
		doPlayerCommand(playerLastCommand);

		if (game.board[29].second.getColor() == Piece::Color::None)
		{
			currentPlayer == &player1 ? logger.log("Switching to Player 2...", Logger::Level::Info) : logger.log("Switching to Player 1...", Logger::Level::Info);
			switchPlayer();
		}
		else
		{
			logger.log("Showing Winner Successfully...", Logger::Level::Info);
			logger.log("Ending Application Successfully...", Logger::Level::Info);
			showWinner();
			std::cin.get();
		}

		//Debug
		//std::cout << "Player command = " << playerLastCommand << " " << std::endl;


		//Debug
		//std::cout << "Winner = " << winner << " " << std::endl;



	}

}

void GameManager::switchPlayer()
{
	// Switching a pointer from player1 to player2 and vice-versa
	currentPlayer = (currentPlayer == &player1 ? &player2 : &player1);
}

std::string GameManager::getPlayerInput() const
{
	// Switching a pointer from player1 to player2 and vice-versa
	// Gets the playerInput

	std::string playerInput;
	std::getline(std::cin, playerInput);
	return playerInput;
}

void GameManager::doPlayerCommand(std::string &playerLastCommand)
{
    std::ofstream of("Pylos.log", std::ios::app);
	Logger logger(of, Logger::Level::Info);
	//based on the input string do commands like move from x to y 
	// Ex:  place 
	// Ex:  take 
	// Ex:  move 
	auto ifSomethingGoesWrong = [&](const std::string &costumMessage)
	{
			std::cout << costumMessage << std::endl;
			std::cout << "There are no movable pieces, Choose another command!" << std::endl;
			std::cout << "Correct commands : place | take | move | help | exit" << std::endl;
			std::cout << "Enter a command: ";
			playerLastCommand = getPlayerInput();
			doPlayerCommand(playerLastCommand);
	};

	// Clears the white space in the command
	playerLastCommand.erase(std::remove_if(playerLastCommand.begin(), playerLastCommand.end(), isspace), playerLastCommand.end());
	// Transforming to lower cases
	std::transform(playerLastCommand.begin(), playerLastCommand.end(), playerLastCommand.begin(), ::tolower);

	if (playerLastCommand == "exit")
	{
		logger.log("Exit Called...", Logger::Level::Info);
		exit(EXIT_FAILURE);
	}
	else if (playerLastCommand == "place")
	{
		if (currentPlayer->countPieces == 0)
		{
		ifSomethingGoesWrong("You have 0 pieces, Choose another command!");
		return;
		}
		std::cout << "Enter where to put the piece: ";
		coordinates = getPlayerInput();
		pieceNewPos = getIndexFromCoordinates(coordinates);
		placePieceFromReserve(pieceNewPos);
	}
	else if (playerLastCommand == "take")
	{
		if (game.checkForTakeablePieces(currentPlayer->color) == false)
		{
		ifSomethingGoesWrong("There are no more movable pieces, Choose another command!");
		return;
		}

		std::string howMany_string;
		std::cout << "Enter how many pieces do you want to take from the board: ";
		howMany_string = getPlayerInput();
		int howMany = std::stoi(howMany_string);
		while (howMany > 2 || howMany < 1)
		{
			std::cout << "Wrong, You can only take 1 or 2 pieces from the board" << std::endl;
			std::cout << "Enter how many pieces do you want to take from the board: ";
			std::cin >> howMany;
		}
		for (int i = 0; i < howMany; ++i)
		{
		std::cout << "Enter the piece you want to take: ";
		coordinates = getPlayerInput();
		pieceNewPos = getIndexFromCoordinates(coordinates);
		takePiecesFromBoard(pieceNewPos);
		if (game.checkForTakeablePieces(currentPlayer->color) == false)
		{
			std::cout << "There are no more movable pieces left, switching player..." << std::endl;
			std::cin.get();
			return;
		}

		if (i == 0 && howMany == 2)
		{
			system("cls");
			game.outputAllLayers();
			std::cout << "It's Player " << currentPlayer->number << "'s turn." << std::endl;
			std::cout << "Current player pieces = " << currentPlayer->countPieces << " " << std::endl;
		}

		}
	}
	else if (playerLastCommand == "move")
	{
		if (game.checkForTakeablePieces(currentPlayer->color) == false)
		{
			ifSomethingGoesWrong("There are no movable pieces, Choose another command!");
			return;
		}
		std::cout << "Enter the piece you want to move: ";
		coordinates = getPlayerInput();
		pieceOldPos = getIndexFromCoordinates(coordinates);

		if (checkMoveablePiece(pieceOldPos) == false)
		{
			ifSomethingGoesWrong("There are no movable pieces, Choose another command!");
			return;
		}

		std::cout << "Enter where do you want to move the piece: ";
		coordinates = getPlayerInput();
		pieceNewPos = getIndexFromCoordinates(coordinates);
		movePieceFromBoardToUpperLevel(pieceOldPos,pieceNewPos);
	}
	else if (playerLastCommand == "help")
	{
		system("cls");
		std::cout << "Hi and Welcome to Pylos \nThis game's purpose is to get first to the top of the pyramid\n";
		std::cout << "Whenever the game asks you what piece do you want to move/take/place, please respect the correct coordinates form." << std::endl;
		std::cout << "The correct form is: row, column, level " << std::endl;
		std::cout << "Press enter to exit from help \nHave Fun && Enjoy ";
		std::cin.get();
		switchPlayer();
	}
	else
	{
		std::cout << "Wrong command entered, Correct commands: place | take | move | help | exit" << std::endl;
		std::cout << "Enter a command: ";
		playerLastCommand = getPlayerInput();
		doPlayerCommand(playerLastCommand);
	}

}

void GameManager::refreshIndexFromNewCoordinates(int & pieceNewIndex, const std::string & errorMessage)
{
	std::ofstream of("Pylos.log", std::ios::app);
	Logger logger(of, Logger::Level::Warning);
	logger.log("Wrong coordinates, refreshing...", Logger::Level::Warning);

	std::cout << errorMessage;
	coordinates = getPlayerInput();
	pieceNewIndex = getIndexFromCoordinates(coordinates);
}

int GameManager::getIndexFromCoordinates(std::string& input_coordinates)
{
	int row = -1, col = -1, level = 0, index;
	int diff[4] = { 0, 16, 25, 29 };
	int nextRowIndex[4] = {4, 3, 2, 1 };

	std::regex correctCoordinatesRegex(R"((\s*[1234]{1}\s*\,{0,1}){2}\s*[1234]{1}\s*)");
	auto getRowColLevel = [&]()
	{
		std::regex isDecimal(R"(\d)");
		std::regex_token_iterator<std::string::iterator> regexIterator(input_coordinates.begin(), input_coordinates.end(), isDecimal);
		row = std::stoi(regexIterator++->str());
		col = std::stoi(regexIterator++->str());
		level = std::stoi(regexIterator++->str());
	};

	if (std::regex_match(input_coordinates, correctCoordinatesRegex))
	{
		getRowColLevel();
	}

	while (!(row <= nextRowIndex[level - 1]) || !(col <= nextRowIndex[level - 1]) || !std::regex_match(input_coordinates, correctCoordinatesRegex))
	{
		std::cout << "Wrong coordinates! \n Please enter correct coordinates:";
		input_coordinates = getPlayerInput();
		std::cout << std::endl;
		if (std::regex_match(input_coordinates, correctCoordinatesRegex))
		getRowColLevel();
	}

	// Debugging
//	std::cout << "Row: " << row << std::endl;
	// Debugging
//	std::cout << "Column: " << col << std::endl;
	// Debugging
//	std::cout << "Level: " << level << std::endl;
	// Debugging
//	std::cout << "Index: " << index << std::endl;

	index = diff[level - 1] + nextRowIndex[level - 1] * (row - 1) + (col - 1);
	return index;
}

void GameManager::movePieceFromBoardToUpperLevel(int pieceOldPos, int pieceNewPos)
{
	// Daca o piesa deasupra lu pieceOldPos == available
	std::ofstream of("Pylos.log", std::ios::app);
	Logger logger(of, Logger::Level::Info);
	std::string LoggerMessage = "Moving piece from index ";

	bool goodPieceChosen = 0;

	if (game.board[pieceOldPos].first.availability != true || game.board[pieceOldPos].second.getColor() != currentPlayer->color || !checkMoveablePiece(pieceOldPos))
	{
		while (goodPieceChosen == 0)
		{
			refreshIndexFromNewCoordinates(pieceOldPos, "Piece cannot be moved, Choose another one: ");
			goodPieceChosen = (game.board[pieceOldPos].first.availability == true && game.board[pieceOldPos].second.getColor() == currentPlayer->color && checkMoveablePiece(pieceOldPos));
		}
	}
	game.board[pieceOldPos].second.setColor(Piece::Color::None);

	if (game.board[pieceNewPos].first.availability != true || game.board[pieceNewPos].second.getColor() != Piece::Color::None || game.board[pieceNewPos].first.level <= game.board[pieceOldPos].first.level || game.board[pieceNewPos].first.level - game.board[pieceOldPos].first.level > 2)
	{
		goodPieceChosen = 0;
		while (goodPieceChosen == 0)
		{
			refreshIndexFromNewCoordinates(pieceNewPos, "Invalid location to move piece, Choose another location: ");
			goodPieceChosen = (game.board[pieceNewPos].first.availability == true && game.board[pieceNewPos].second.getColor() == Piece::Color::None  && game.board[pieceOldPos].first.level < game.board[pieceNewPos].first.level && game.board[pieceNewPos].first.level - game.board[pieceOldPos].first.level <= 2);
		}
	}

	LoggerMessage = std::to_string(pieceOldPos) + " to index " + std::to_string(pieceNewPos);

	logger.log(LoggerMessage, Logger::Level::Info);
	currentPlayer == &player1 ? game.board[pieceNewPos].second.setColor(Piece::Color::White) : game.board[pieceNewPos].second.setColor(Piece::Color::Black);
	fixSquareBelow(pieceNewPos, false);
}

void GameManager::takePiecesFromBoard(int pieceNewPos)
{
	std::ofstream of("Pylos.log", std::ios::app);
	Logger logger(of, Logger::Level::Info);
	std::string LoggerMessage = "Taking piece from index ";

		bool goodPieceChosen = 0;
		if (game.board[pieceNewPos].first.availability != true || game.board[pieceNewPos].second.getColor() != currentPlayer->color)
			{
				while (goodPieceChosen == 0)
				{
					refreshIndexFromNewCoordinates(pieceNewPos, "Piece cannot be taken, Choose another one: ");
					goodPieceChosen = (game.board[pieceNewPos].first.availability == true && game.board[pieceNewPos].second.getColor() == currentPlayer->color);
				}
			}

			LoggerMessage += std::to_string(pieceNewPos);
			logger.log(LoggerMessage, Logger::Level::Info);

			game.board[pieceNewPos].second.setColor(Piece::Color::None);
			currentPlayer->countPieces++;
			fixSquareBelow(pieceNewPos, true);
}

void GameManager::placePieceFromReserve(int pieceNewPos) 
{
	std::ofstream of("Pylos.log", std::ios::app);
	Logger logger(of, Logger::Level::Info);
	std::string LoggerMessage = "Placing piece on index ";


	bool goodPieceChosen = 0;
	if (game.board[pieceNewPos].first.availability != true || game.board[pieceNewPos].second.getColor() != Piece::Color::None)
	{
		goodPieceChosen = 0;
		while (goodPieceChosen == 0)
		{
			refreshIndexFromNewCoordinates(pieceNewPos, "Invalid location, Choose another one: ");
			goodPieceChosen = (game.board[pieceNewPos].first.availability == true && game.board[pieceNewPos].second.getColor() == Piece::Color::None);
		}
	}
	LoggerMessage += std::to_string(pieceNewPos);
	logger.log(LoggerMessage, Logger::Level::Info);

	currentPlayer == &player1 ? game.board[pieceNewPos].second.setColor(Piece::Color::White) : game.board[pieceNewPos].second.setColor(Piece::Color::Black);
	currentPlayer->countPieces--;
	fixSquareBelow(pieceNewPos, false);
}

void GameManager::fixSquareBelow(int upperPieceIndex, bool newAvailability)
{
	if (upperPieceIndex > 15)
	{
		int nextRowIndex[4] = { 4, 3, 2, 1 };
		int pieceCount[4] = { 16, 9, 4, 1 };
		int squareDownIndex = upperPieceIndex - 1 - pieceCount[game.board[upperPieceIndex].first.level - 1 - 1] + game.board[upperPieceIndex].first.row;
		if (newAvailability == true)
		{
			game.board[squareDownIndex].first.availability = true;
			game.board[squareDownIndex + 1].first.availability = true;
			game.board[squareDownIndex + nextRowIndex[game.board[squareDownIndex].first.level - 1]].first.availability = true;
			game.board[squareDownIndex + 1 + nextRowIndex[game.board[squareDownIndex].first.level - 1]].first.availability = true;
		}
		else // newAvailability == false
		{
			game.board[squareDownIndex].first.availability = false;
			game.board[squareDownIndex + 1].first.availability = false;
			game.board[squareDownIndex + nextRowIndex[game.board[squareDownIndex].first.level - 1]].first.availability = false;
			game.board[squareDownIndex + 1 + nextRowIndex[game.board[squareDownIndex].first.level - 1]].first.availability = false;
		}
	}
	else
		return;
}


void GameManager::showWinner() const
{
	foundWinner = true;
	std::cout << (game.board[29].second.getColor() == Piece::Color::Black ? "Black is Winner" : "White is Winner");
}

bool GameManager::checkMoveablePiece(int pieceIndex) const
{
	if (!game.checkForTakeablePieces(game.board[pieceIndex].second.getColor()))
		return false;
	int levelIndex[3] = { 16, 25, 29 };

	for (int i = levelIndex[game.board[pieceIndex].first.level - 1]; i < levelIndex[2]; ++i)
	{
		if (game.board[i].second.getColor() == Piece::Color::None && game.board[i].first.availability == true)
			return true;
	}
	return false;



}




